#import <Foundation/NSObject.h>

@interface Node : NSObject
{
@private
   Node* prev;
   int  x;
}

- (id) prev: (Node*) prev_value;
- (Node*) prev;
- (id) x: (int) x_value;
- (int) x;
@end