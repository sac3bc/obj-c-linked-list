#import "Node.h"

@implementation Node

- (id) prev: (Node*) prev_value
{
   prev = prev_value;
   return self;
}

- (Node*) prev
{
   return prev;
}

- (id) x: (int) x_value
{
   x = x_value;
   return self;
}

- (int) x
{
   return x;
}


@end