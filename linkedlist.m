#import "Node.h"
#import <stdio.h>

int main(void)
{

  
  int itemNum;
  printf("Enter how many values to input: ");
  scanf("%d", &itemNum);

  Node * savedNode = [Node new];

  for (int i = 0; i < itemNum; i++) {

    int value;
    printf("Enter value %d: \n", i + 1);
    scanf("%d",&value);
    

    Node * current = [Node new];

    [current x: value]; 

    if(i == 0){
      [current prev: nil];
    }
    else{
      [current prev: savedNode];
    }

    savedNode = current; 

  }